<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// agenda router
$router->get('agenda', 'AgendaController@index');
$router->post('addagenda', 'AgendaController@store');
$router->put('editagenda/{agenda_id}', 'AgendaController@update');
$router->delete('deleteagenda/{agenda_id}', 'AgendaController@destroy');
$router->get('agendaactivation/{agenda_id}', 'AgendaController@activation');
$router->get('viewagenda', 'AgendaController@view');
$router->get('viewagenda/{agenda_id}', 'AgendaController@viewById');


// Event router
$router->get('event', 'EventController@index');
$router->post('addevent', 'EventController@store');
$router->post('editevent', 'EventController@update');
$router->delete('deleteevent/{event_id}', 'EventController@destroy');
$router->get('eventactivation/{event_id}', 'EventController@activation');
$router->get('viewevent', 'EventController@viewByEventCode');
$router->get('viewevent/{event_code}', 'EventController@view');

// Ticket router
$router->get('ticket', 'TicketController@index');
$router->post('addticket', 'TicketController@store');
$router->get('ticketactivation/{ticket_id}', 'TicketController@activation');
$router->get('viewticket/{ticket_id}', 'TicketController@view');
$router->get('viewticket', 'TicketController@viewDecode');
$router->get('search/find', 'TicketController@search');
$router->delete('deleteticket/{ticket_id}', 'TicketController@destroy');
$router->get('viewmember/{member_id}', 'TicketController@viewByMember');

// Galery router
$router->get('galery', 'GaleryController@index');
$router->post('addgalery', 'GaleryController@store');
$router->post('editgalery/{galery_id}', 'GaleryController@update');
$router->delete('deletegalery/{galery_id}', 'GaleryController@destroy');
$router->get('galeryactivation/{galery_id}', 'GaleryController@activation');
$router->get('viewgalery', 'GaleryController@viewDecode');
$router->get('viewgalery/{galery_id}', 'GaleryController@view');

// Document router
$router->get('document', 'DocumentController@index');
$router->post('adddocument', 'DocumentController@store');
$router->post('editdocument/{document_id}', 'DocumentController@update');
$router->delete('deletedocument/{document_id}', 'DocumentController@destroy');
$router->get('documentactivation/{document_id}', 'DocumentController@activation');
$router->get('viewdocument', 'DocumentController@viewDecode');
$router->get('viewdocument/{document_id}', 'DocumentController@view');

//News router
$router->get('news', 'NewsController@index');
$router->post('addnews', 'NewsController@store');
$router->post('editnews/{news_id}', 'NewsController@update');
$router->delete('deletenews/{news_id}', 'NewsController@destroy');
$router->get('newsactivation/{news_id}', 'NewsController@newsactivation');
$router->get('viewnews/{news_id}', 'NewsController@view');
$router->get('viewnews', 'NewsController@viewDecode');

//Souvenir router
$router->get('souvenir', 'SouvenirController@index');
$router->post('addsouvenir', 'SouvenirController@store');
$router->get('actsouvenir/{souvenir_id}', 'SouvenirController@activation');
$router->get('viewsouvenir/{souvenir_id}', 'SouvenirController@view');
$router->get('viewsouvenir', 'SouvenirController@viewByEvent');
$router->delete('deletesouvenir/{souvenir_id}', 'SouvenirController@destroy');
$router->post('editsouvenir/{souvenir_id}', 'SouvenirController@update');

//Quiz router
$router->get('quiz', 'QuizController@index');
$router->get('viewquiz', 'QuizController@viewDecode');
$router->get('viewquiz/{quiz_id}', 'QuizController@view');
$router->post('addquiz', 'QuizController@store');
$router->delete('deletequiz/{quiz_id}', 'QuizController@destroy');
$router->get('quizactivation/{quiz_id}','QuizController@activation');

//Question router
$router->get('question', 'QuestionController@index');
$router->get('viewquestion/{question_id}', 'QuestionController@view');
$router->post('addquestion/{quiz_code}', 'QuestionController@store');
$router->delete('deletequestion/{question_id}', 'QuestionController@destroy');
$router->get('questionactivation/{question_id}','QuestionController@activation');
$router->post('editquestion/{question_id}', 'QuestionController@update');
$router->get('viewbyquiz/{quiz_code}', 'QuestionController@viewByQuiz' );

//Answer router
$router->get('answer', 'AnswerController@index');
$router->get('viewanswer/{question_id}', 'AnswerController@viewByQuestion');
$router->get('viewanswerid/{answer_id}', 'AnswerController@view');
$router->post('addanswer/{question_id}', 'AnswerController@store');
$router->delete('deleteanswer/{answer_id}', 'AnswerController@destroy');
$router->get('answeractivation/{answer_id}', 'AnswerController@activation');

//Feedback router
$router->get('feedback', 'FeedbackController@index');
$router->get('viewfeedback', 'FeedbackController@view');
$router->post('addfeedback', 'FeedbackController@store');
$router->post('editfeedback/{feedback_id}', 'FeedbackController@update');
$router->get('activationfeedback/{feedback_id}', 'FeedbackController@activation');
$router->delete('deletefeedback/{feedback_id}', 'FeedbackController@destroy');

//Feedback Data router
$router->get('fd', 'FeedbackDataController@index');
$router->get('viewfd', 'FeedbackDataController@viewDecode');
$router->get('viewfd/{feedback_data_id}', 'FeedbackDataController@view');
$router->post('addfd', 'FeedbackDataController@store');
$router->post('editfd/{feedback_data_id}', 'FeedbackDataController@update');
$router->get('activationfd/{feedback_data_id}', 'FeedbackDataController@activation');
$router->delete('deletefd/{feedback_data_id}', 'FeedbackDataController@destroy');