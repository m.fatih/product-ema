<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket', function (Blueprint $table) {
            $table->bigIncrements('ticket_id');
            $table->string('ticket_number')->nullable();
            $table->string('event_code')->nullable();
            $table->string('event_title')->nullable();
            $table->string('event_location')->nullable();
            $table->string('event_start')->nullable();
            $table->string('event_end')->nullable();
            $table->string('member_id')->nullable();
            $table->string('member_name')->nullable();
            $table->string('member_email')->nullable();
            $table->string('member_phone_number')->nullable();
            $table->string('member_image')->nullable();
            $table->enum('status', ['Active', 'NonActive'])->default('NonActive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket');
    }
}
