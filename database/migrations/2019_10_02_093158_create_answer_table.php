<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer', function (Blueprint $table) {
            $table->bigIncrements('answer_id');
            $table->string('question_id')->nullable();
            $table->string('member_id')->nullable();
            $table->string('jawaban_member')->nullable();
            $table->integer('point_member')->nullable();
            $table->integer('waktu_member')->nullable();
            $table->enum('status', ['Active', 'NonActive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer');
    }
}
