<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda', function (Blueprint $table) {
            $table->bigIncrements('agenda_id');
            $table->string('agenda_code')->nullable();
            $table->string('event_code')->nullable();
            $table->string('event_title')->nullable();
            $table->string('event_duration')->nullable();
            $table->string('agenda_title')->nullable();
            $table->longText('agenda_desc')->nullable();
            $table->string('event_month')->nullable();
            $table->string('event_year')->nullable();
            $table->string('event_date')->nullable();
            $table->string('event_time')->nullable();
            $table->string('event_speaker')->nullable();
            $table->string('event_location_detail')->nullable();
            $table->enum('status', ['Active', 'NonActive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenda');
    }
}
