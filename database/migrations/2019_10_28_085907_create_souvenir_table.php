<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSouvenirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('souvenir', function (Blueprint $table) {
            $table->bigIncrements('souvenir_id');
            $table->string('event_code')->nullable();
            $table->string('souvenir_code')->nullable();
            $table->string('title')->nullable();
            $table->string('desc')->nullable();
            $table->string('image')->nullable();
            $table->enum('status', ['Active', 'NonActive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('souvenir');
    }
}
