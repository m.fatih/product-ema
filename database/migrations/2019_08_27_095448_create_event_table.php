<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event', function (Blueprint $table) {
            $table->bigIncrements('event_id');
            $table->string('event_code');
            $table->string('event_title')->nullable();
            $table->string('event_category')->nullable();
            $table->longText('event_desc')->nullable();
            $table->string('event_location')->nullable();
            $table->string('event_latitude')->nullable();
            $table->string('event_longitude')->nullable();
            $table->string('event_duration')->nullable();
            $table->string('event_image')->nullable();
            $table->string('background_color')->nullable();
            $table->string('icon_color')->nullable();
            $table->timestamp('event_start')->nullable();
            $table->timestamp('event_end')->nullable();
            $table->enum('status', ['Active', 'NonActive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event');
    }
}
