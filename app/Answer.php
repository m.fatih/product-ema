<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;


class Answer extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'answer_id';
    protected $table = 'answer';
    protected $fillable = [
        'question_id', 
        'member_id',
        'jawaban_member',
        'point_member',
        'waktu_member',
        'status'  
    ];

    

   
}