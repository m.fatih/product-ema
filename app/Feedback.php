<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;


class Feedback extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'feedback_id';
    protected $table = 'feedback';
    protected $fillable = [
        'event_code',
        'desc',
        'status'  
    ];

    

   
}