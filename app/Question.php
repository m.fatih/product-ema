<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;


class Question extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'question_id';
    protected $table = 'question';
    protected $fillable = [
        'quiz_code', 
        'pertanyaan',
        'jawaban_1',
        'jawaban_2',
        'jawaban_3',
        'jawaban_4',
        'jawaban_benar',
        'point',
        'time',
        'status'  
    ];

    public function quiz()
    {
        return $this->belongsTo(Quiz::class, 'soal_id');
    }

   
}