<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;


class Event extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'event_id';
    protected $table = 'event';
    protected $fillable = [
        'event_code',
        'event_title',
        'event_category',
        'event_desc',
        'event_location',
        'event_latitude',
        'event_longitude',
        'event_duration',
        'event_image',
        'bacground_color',
        'icon_color',
        'event_start',
        'event_end',
        'status'
    ];

    protected $dates = [
        'event_start',
        'event_end',
    ];

   
}
