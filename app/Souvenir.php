<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;


class Souvenir extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'souvenir_id';
    protected $table = 'souvenir';
    protected $fillable = [
        'event_code',
        'souvenir_code',
        'title',
        'desc', 
        'image',
        'status'
    ];

   
}