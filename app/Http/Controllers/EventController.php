<?php

namespace App\Http\Controllers;
use App\Event;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;

class EventController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    private function getCodeEvent()
    {
        $prefix = 'EVENT';

        $sNextKode = "";
        $sLastKode = "";
        $value = Event::orderBy('event_id', 'desc')->first();
        if ($value != "") { // jika sudah ada, langsung ambil dan proses...
            $sLastKode = intval(substr($value->event_code, 5)); // ambil 3 digit terakhir
            $sLastKode = intval($sLastKode) + 1; // konversi ke integer, lalu tambahkan satu
            $sNextKode = $prefix.date('ym').substr($sLastKode,4); // format hasilnya dan tambahkan prefix
        } else { // jika belum ada, gunakan kode yang pertama
            $sNextKode = $prefix.date('ym').'0001';
        }
        return $sNextKode;

        //return $prefix.'0001';
    }

    public function index(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $event = Event::all();
        if ($event)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $event,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function store(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }

    try{
        $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
    } 
    
    catch(\Exception $e){
        return response()->json([
            'StatusCode'=>'400',
            'Error'=>true,
            'Message'=>'Failed, Token Is Wrong!'
        ],422);
    }

        

        $event = new Event;
        $event->event_code = $this->getCodeEvent();
        $event->event_title = $request->event_title;
        $event->event_category = $request->event_category;
        $event->save();
        
        if ($event)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $event,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }
    
    public function update(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
       
        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
        
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }

        $event = Event::where('event_code', $decodeToken->event_code)->first();

        if($request->event_image != null)
        {
            $event_image = $request->file('event_image');
            $new_name = time().'-'.$event_image->getClientOriginalName();
            $event_image->move('image/event', $new_name);

            $event->event_title = $request->event_title;
            $event->event_desc = $request->event_desc;
            $event->event_latitude = $request->event_latitude;
            $event->event_longitude = $request->event_longitude;
            $event->event_location = $request->event_location;
            $event->event_duration = $request->event_duration;
            $event->event_image = $new_name;
            $event->background_color = $request->background_color;
            $event->icon_color = $request->icon_color;
            $event->event_start = $request->event_start;
            $event->event_end = $request->event_end;
            
        }

        else
        {
            $event->event_title = $request->event_title;
            $event->event_desc = $request->event_desc;
            $event->event_latitude = $request->event_latitude;
            $event->event_longitude = $request->event_longitude;
            $event->event_location = $request->event_location;
            $event->event_duration = $request->event_duration;
            $event->background_color = $request->background_color;
            $event->icon_color = $request->icon_color;
            $event->event_start = $request->event_start;
            $event->event_end = $request->event_end;
        }        
        
        // dd($event);
        $event->save();

        if ($event)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $event,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function destroy(Request $request, $event_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
        
        $event = Event::find($event_id);
        
        if ($event != null )
        {
            
            $event->delete();
            return response()->json([
                'StatusCode' => 200,
                'Error' => false,
                'Message' => 'success',
                
            ]);
        }
        elseif ($event == null )
        {
            return response()->json([   
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function activation(Request $request, $event_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $event = Event::find($event_id);
        if ($event != null)
        {
        if ($event->status  == 'Active')
        {
            $event->status = 'NonActive';
            $event->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'NonActivation Success',
            ]);
        }
        elseif ($event->status  == 'NonActive')
        {
            $event->status = 'Active';
            $event->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'Activation Success',
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }
        elseif ($event == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function viewByEventCode(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
       
        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
        
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }
        
        $event = Event::where('event_code', $decodeToken->event_code)->first();
        if ($event)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $event,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function view(Request $request, $event_code)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
       

        $event = Event::where('event_code', $event_code)->first();
        if ($event != null)
        {
        if ($event)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $event,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }
        elseif ($event == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }
}
