<?php

namespace App\Http\Controllers;
use App\Ticket;
use App\Event;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;
use Auth;

class TicketController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    private function getNoTicket()
    {
        $prefix = 'TICKET';

        $sNextKode = "";
        $sLastKode = "";
        $value = Ticket::orderBy('ticket_id', 'desc')->first();
        if ($value != "") { // jika sudah ada, langsung ambil dan proses...
            $sLastKode = intval(substr($value->ticket_number, 6)); // ambil 3 digit terakhir
            $sLastKode = intval($sLastKode) + 1; // konversi ke integer, lalu tambahkan satu
            $sNextKode = $prefix.date('ym').substr($sLastKode,4); // format hasilnya dan tambahkan prefix
        } else { // jika belum ada, gunakan kode yang pertama
            $sNextKode = $prefix.date('ym').'0001';
        }
        return $sNextKode;

        //return $prefix.'0001';
    }

    public function index(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $ticket = Ticket::all();
        if ($ticket)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $ticket,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function store(Request $request)
    {
        
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
    
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }
        // dd($token);

        $ticket = new Ticket;
        $ticket->ticket_number = $this->getNoTicket();
        $ticket->event_code = $decodeToken->event_code;
        // dd($ticket);
        $ticket->event_title = $request->event_title;
        
        $ticket->event_location = $request->event_location;
        // dd($ticket);
        $ticket->event_start = $request->event_start;
        $ticket->event_end = $request->event_end;
        $ticket->member_id = $request->member_id;
        // dd($ticket);
        $ticket->member_name = $request->member_name;
        // dd($ticket);
        $ticket->member_email = $request->member_email;
        // dd($ticket);
        $ticket->member_phone_number = $request->member_phone_number;
        // dd($ticket);
        $ticket->save();
        
        if ($ticket)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $ticket,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function activation(Request $request, $ticket_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $ticket = Ticket::find($ticket_id);
        if ($ticket != null)
        {
        if ($ticket->status  == 'NonActive')
        {
            $ticket->status = 'Active';
            $ticket->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'Active Success',
            ]);
        }
        elseif ($ticket->status  == 'Active')
        {
            $ticket->status = 'Active';
            $ticket->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'Ticket is Acitve',
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
        }
        elseif ($souvenir == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function view(Request $request, $ticket_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $ticket = Ticket::where('ticket_id', $ticket_id)->first();
        if ($ticket != null)
        {
        if ($ticket)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $ticket,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
        }
        elseif ($ticket == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function viewDecode(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
    
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }

        $ticket = Ticket::where('event_code', $decodeToken->event_code)->get();
        if ($ticket)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $ticket,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function search(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
    
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }

        
        // dd($event_code);
        $find = $request->find;

        $ticket = Ticket::where('event_code', $decodeToken->event_code);
        // dd($decodeToken);
        if($find)
        {
            $ticket = $ticket->where(function($query) use($find){
                $query
                ->where ('member_name', 'like', "%". $find . "%")
                ->orWhere ('member_email', 'like', "%". $find . "%")
                ->orWhere ('member_phone_number', 'like', "%". $find . "%");
            });
        }
        
        // dd($ticket);

        if($ticket){
            return response()->json([
                'StatusCode' => 200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $ticket->get(),

            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function destroy(Request $request, $ticket_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $ticket = Ticket::where('ticket_id', $ticket_id)->first();
        if ($ticket != null )
        {
            
            $ticket->delete();

        
            return response()->json([
                'StatusCode' => 200,
                'Error' => false,
                'Message' => 'success',
                
            ]);
        }
        elseif ($ticket == null )
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
        
        
    }

    public function viewbyMember(Request $request, $member_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $ticket = Ticket::where('member_id', $member_id)->first();
        if ($ticket != null )
        {
            

        
            return response()->json([
                'StatusCode' => 200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $ticket
                
            ]);
        }
        elseif ($ticket == null )
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Member Tidak Tersedia',
                
            ]);
        }
        
        
    }
    
}
