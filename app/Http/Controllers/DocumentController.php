<?php

namespace App\Http\Controllers;
use App\Document;
use App\Event;
use Illuminate\Http\Request;
use DB;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class DocumentController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    public function index(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $document = Document::all();
        if ($document)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $document,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function view(Request $request, $document_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $document = Document::where('document_id', $document_id)->first();
        if ($document != null)
        {
        if ($document)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $document,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
        }
        elseif ($document == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function viewDecode(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
        
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }

        $document = Document::where('event_code', $decodeToken->event_code)->get();
        if ($document)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $document,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function store(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
        
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }

        $document = new Document;

        $event_file = $request->file('event_file');
        $new_file = time().'-'.$event_file->getClientOriginalName();
        $event_file->move('file', $new_file);

        $document->event_code = $decodeToken->event_code;
        $document->event_file = $new_file;
        $document->file_desc = $request->file_desc;
        $document->save();
        
        if ($document)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $document,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }

    }
    
    public function update(Request $request, $document_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $document = Document::where('document_id', $document_id)->first();
        
        if ($document != null)
        {
        if ($request->event_file != null )
        {
            $event_file = $request->file('event_file');
            $new_file = str_random(8).'-'.$event_file->getClientOriginalName();
            $event_file->move('file/', $new_file);

            $document->event_file = $new_file;
            $document->file_desc = $request->file_desc;
        }
        else
        {
            $document->file_desc = $request->file_desc;
        }
        
        $document->save();
        

        if ($document)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $document,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
        }
        elseif ($document == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function destroy(Request $request, $document_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
        
        
        $document = Document::where('document_id', $document_id)->first();
        
        if ($document != null)
        {
            $document->delete();
        
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
            ]);
        
        }
    
        elseif ($document == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function activation(Request $request, $document_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $document = document::find($document_id);

        if ($document != null)
        {
        
        if ($document->status  == 'Active')
        {
            $document->status = 'NonActive';
            $document->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'NonActivation Success',
            ]);
        }
        elseif ($document->status  == 'NonActive')
        {
            $document->status = 'Active';
            $document->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'Activation Success',
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }
        elseif ($document == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    
    }
}
