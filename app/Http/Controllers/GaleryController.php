<?php

namespace App\Http\Controllers;
use App\Event;
use App\Galery;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;

class GaleryController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    public function index(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $galery = Galery::all();
        if ($galery)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $galery,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function viewDecode(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
       
        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
        
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }

        $galery = Galery::where('event_code', $decodeToken->event_code)->get();
        if ($galery)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $galery,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function view(Request $request, $galery_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $galery = Galery::where('galery_id', $galery_id)->first();
        if ($galery != null)
        {
        if ($galery)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $galery,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
        }
        elseif ($galery == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function store(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
       
        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
        
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }

        // dd($decodeToken);

        $event_image = $request->file('galery_image');

        $new_name = time().'-'.$event_image->getClientOriginalName();
        
        $event_image->move('image/galery', $new_name);

        
        $galery = new Galery;
        $galery->event_code = $decodeToken->event_code;
        $galery->galery_title = $request->galery_title;
        $galery->galery_image = $new_name;
        $galery->galery_desc = $request->galery_desc;
        $galery->save();
        
        if ($galery)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $galery,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }
    
    public function update(Request $request, $galery_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        
        $galery = Galery::where('galery_id', $galery_id)->first();

        if ($galery != null)
        {

        if ($request->galery_image != null)
        {
            $galery_image = $request->file('galery_image');
            $new_name = time().'-'.$galery_image->getClientOriginalName();
            $galery_image->move('image/galery', $new_name);
            
            $galery->galery_title = $request->galery_title;
            $galery->galery_image = $new_name;
            $galery->galery_desc = $request->galery_desc;
        }
        else
        {
            $galery->galery_title = $request->galery_title;
            $galery->galery_desc = $request->galery_desc;
        }
        
        $galery->save();

        if ($galery)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $galery,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
        }
        elseif ($galery == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function destroy(Request $request, $galery_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
        
        $galery = Galery::find($galery_id);

        if ($galery != null )
        {
            
            $galery->delete();
        
        
            return response()->json([
                'StatusCode' => 200,
                'Error' => false,
                'Message' => 'success',
                
            ]);
        }
        elseif ($galery == null )
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function activation(Request $request, $galery_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $galery = Galery::find($galery_id);
        
        if($galery != null)
        {
        
        if ($galery->status  == 'Active')
        {
            $galery->status = 'NonActive';
            $galery->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'NonActivation Success',
            ]);
        }
        elseif ($galery->status  == 'NonActive')
        {
            $galery->status = 'Active';
            $galery->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'Activation Success',
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
        }
        elseif($galeri == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }
}
