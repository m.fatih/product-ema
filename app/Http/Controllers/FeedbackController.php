<?php

namespace App\Http\Controllers;
use App\Feedback;
use App\Event;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class FeedbackController extends Controller
{
    public function index(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
        
        $feedback = Feedback::all();
        
        if ($feedback)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $feedback,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function view(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
    
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }

        $feedback = Feedback::where('event_code', $decodeToken->event_code)->first();
        
        if ($feedback)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $feedback,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
        
    }

    public function store(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
    
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }

        $feedback = new Feedback;
        $feedback->event_code = $decodeToken->event_code;
        $feedback->desc = $request->desc;
        $feedback->save();

        if ($feedback)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $feedback,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function update(Request $request, $feedback_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $feedback = Feedback::where('feedback_id', $feedback_id)->first();
        if ($feedback != null)
        {
        $feedback->desc = $request->desc;
        $feedback->save();

        if ($feedback)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $feedback,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }
        elseif ($feedback == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function activation(Request $request, $feedback_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $feedback = Feedback::where('feedback_id', $feedback_id)->first();
        
        if ($feedback != null)
        {
            
        if ($feedback->status  == 'Active')
        {
            $feedback->status = 'NonActive';
            $feedback->save();
            
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'NonActive Success',
            ]);
        }
        elseif ($feedback->status  == 'NonActive')
        {
            $feedback->status = 'Active';
            $feedback->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'Active success',
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }
        elseif ($feedback == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }
    
    public function destroy(Request $request, $feedback_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
        

       $feedback = Feedback::where('feedback_id', $feedback_id)->first();
        
       if ($feedback != null)
       {
            $feedback->delete();
        
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                
            ]);
        }
        elseif ($feedback == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    
    

    }
}