<?php

namespace App\Http\Controllers;
use App\Quiz;
use App\Question;
use App\Answer;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use DB;

class AnswerController extends Controller
{
     public function index(Request $request)
     {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }

        $answer = Answer::all();
        if ($answer)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $answer,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function viewByQuestion(Request $request, $question_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }

        $answer = Answer::where('question_id', $question_id)->first();
        if ($answer != null)
        {

        if ($answer)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $answer,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }
        elseif ($answer == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function view(Request $request, $answer_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }

        $answer = Answer::where('answer_id', $answer_id)->first();
        if ($answer != null)
        {

        if ($answer)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $answer,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }
        elseif ($answer == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function store(Request $request, $question_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true, 
                'Message'=>'Token Required',
                ],200);
        }


        $question = Question::where('question_id', $question_id)->first();
        
        $answer = new Answer;
        $answer->question_id = $question->question_id;
        $answer->member_id = $request->member_id;
        $answer->jawaban_member = $request->jawaban_member;
        $answer->point_member = $request->point_member;
        $answer->waktu_member = $request->waktu_member;
        $answer->save();

        if ($answer)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $answer,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }


    public function destroy(Request $request, $answer_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }

        $answer = Answer::where('answer_id', $answer_id)->first();

        if ($answer != null)
        {
            $answer->delete();

        
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success'
            ]);
      
        }
        elseif ($answer == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function activation(Request $request, $answer_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $answer = Answer::find($answer_id);

        if ($answer != null)
        {
        
        if ($answer->status  == 'Active')
        {
            $answer->status = 'NonActive';
            $answer->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'NonActive Success',
            ]);
        }
        elseif ($answer->status  == 'NonActive')
        {
            $answer->status = 'Active';
            $answer->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'Active success',
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }
        elseif ($answer == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }
}