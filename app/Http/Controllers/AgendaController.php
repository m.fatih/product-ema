<?php

namespace App\Http\Controllers;
use App\Agenda;
use App\Event;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class AgendaController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    private function getCodeAgenda()
    {
        $prefix = 'AGENDA';

        $sNextKode = "";
        $sLastKode = "";
        $value = Agenda::orderBy('agenda_id', 'desc')->first();
        if ($value != "") { // jika sudah ada, langsung ambil dan proses...
            $sLastKode = intval(substr($value->agenda_code, 6)); // ambil 3 digit terakhir
            $sLastKode = intval($sLastKode) + 1; // konversi ke integer, lalu tambahkan satu
            $sNextKode = $prefix.date('ym').substr($sLastKode,4); // format hasilnya dan tambahkan prefix
        } else { // jika belum ada, gunakan kode yang pertama
            $sNextKode = $prefix.date('ym').'0001';
        }
        return $sNextKode;

        //return $prefix.'0001';
    }

    public function index(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
        // dd($request);
        $agenda = Agenda::all();
        
        if ($agenda)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $agenda,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function store(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }

        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
        
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }

        
        $event = Event::where('event_code', $decodeToken->event_code)->first();
        
        $agenda = new Agenda;
        $agenda->agenda_code = $this->getCodeAgenda();
        $agenda->event_code = $decodeToken->event_code;
        $agenda->event_title = $event->event_title;
        $agenda->event_duration = $event->event_duration;
        $agenda->agenda_title = $request->agenda_title;
        $agenda->agenda_desc = $request->agenda_desc;
        $agenda->event_month = $request->event_month;
        $agenda->event_year = $request->event_year;
        $agenda->event_date = $request->event_date;
        $agenda->event_time = $request->event_time;
        $agenda->event_speaker = $request->event_speaker;
        $agenda->event_location_detail = $request->event_location_detail;
        // dd($agenda);
        $agenda->save();
        
        if ($agenda)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $agenda,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }

    }
    
    public function update(Request $request, $agenda_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }

        // $headerApiV2 = ['token' => $request->header('token')];
        // dd($request);
        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
    
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }
        
        
        
        $agenda = Agenda::where('agenda_id', $agenda_id)->first();
        
        if ($agenda != null)
        {
        $agenda->agenda_title = $request->agenda_title;
        $agenda->agenda_desc = $request->agenda_desc;
        $agenda->event_month = $request->event_month;
        $agenda->event_year = $request->event_year;
        $agenda->event_date = $request->event_date;
        $agenda->event_time = $request->event_time;
        $agenda->event_speaker = $request->event_speaker;
        $agenda->event_location_detail = $request->event_location_detail;
        $agenda->save();

        if ($agenda)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $agenda,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }
        elseif ($agenda == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function destroy(Request $request, $agenda_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
        
        $agenda = Agenda::find($agenda_id);
        if ($agenda != null)
        {
            $agenda->delete();
        
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                
            ]);
        
    }
        elseif ($agenda == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function view(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
    
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }

        
        $agenda = Agenda::where('event_code', $decodeToken->event_code)->get();

        

        if ($agenda)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $agenda,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }
    public function viewbyId(Request $request, $agenda_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $agenda = Agenda::where('agenda_id', $agenda_id)->first();
        if ($agenda != null)
        {
        if ($agenda)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $agenda,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }
        elseif ($agenda == null)
        {
            return response()->json([
                'StatusCode' => true,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function activation(Request $request, $agenda_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $agenda = Agenda::where('agenda_id', $agenda_id)->first();
        if ($agenda != null)
        {
        if ($agenda->status  == 'Active')
        {
            $agenda->status = 'NonActive';
            $agenda->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'NonActive Success',
            ]);
        }
        elseif ($agenda->status  == 'NonActive')
        {
            $agenda->status = 'Active';
            $agenda->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'Active success',
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }   
        elseif ($agenda == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    } 
}
