<?php

namespace App\Http\Controllers;
use App\News;
use App\Event;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;

class NewsController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    private function getCodeNews()
    {
        $prefix = 'NEWS';

        $sNextKode = "";
        $sLastKode = "";
        $value = News::orderBy('news_id', 'desc')->first();
        if ($value != "") { // jika sudah ada, langsung ambil dan proses...
            $sLastKode = intval(substr($value->news_code, 4)); // ambil 3 digit terakhir
            // dd($sLastKode);
            $sLastKode = intval($sLastKode) + 1; // konversi ke integer, lalu tambahkan satu
            $sNextKode = $prefix.date('ym').substr($sLastKode,4); // format hasilnya dan tambahkan prefix
        } else { // jika belum ada, gunakan kode yang pertama
            $sNextKode = $prefix.date('ym').'0001';
        }
        return $sNextKode;

        //return $prefix.'0001';
    }

    public function index(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $news = News::all();
        if ($news)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $news,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function store(Request $request)
    {
        
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
        
        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
    
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }
        
        $news = new News;
        
        $news_image = $request->file('news_image');
        $image = time().'-'.$news_image->getClientOriginalName();
        $news_image->move('image/news', $image);

        
        $news->news_code = $this->getCodeNews();
        $news->event_code = $decodeToken->event_code;
        $news->news_title = $request->news_title;
        $news->news_image = $image;
        $news->news_desc = $request->news_desc;
        $news->news_creator = $request->news_creator;
        $news->save();
        
        if ($news)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $news,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function update(Request $request, $news_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
        
        // dd('ok');
        $news = News::where('news_id', $news_id)->first();
        if ($news != null)
        {
        if ($request->news_image != null)
        {
            $news_image = $request->file('news_image');
            $new_image = time().'-'.$news_image->getClientOriginalName();
            $news_image->move('image/news', $new_image);

            $news->news_title = $request->news_title;
            $news->news_image = $new_image;
            $news->news_desc = $request->news_desc;
            $news->news_creator = $request->news_creator;
            
        }
        else
        {
            $news->news_title = $request->news_title;
            $news->news_desc = $request->news_desc;
            $news->news_creator = $request->news_creator;
        }

        $news->save();

        if ($news)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $news,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
        }
        elseif ($news == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
        
    }

    public function destroy(Request $request, $news_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
        
        $news = News::find($news_id);
            
        if ($news != null )
        {
            
            $news->delete();
        
            return response()->json([
                'StatusCode' => 200,
                'Error' => false,
                'Message' => 'success',
                
            ]);
        }
        
        elseif ($news == null )
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
        
        
    }

    public function newsactivation(Request $request, $news_id)
    {
        
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $news = News::where('news_id', $news_id)->first();
        if ($news != null)
        {
        if ($news->status  == 'Active')
        {
            $news->status = 'NonActive';
            $news->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'NonActivation Success',
            ]);
        }
        elseif ($news->status  == 'NonActive')
        {
            $news->status = 'Active';
            $news->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'Activation Success',
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
        }
        elseif($news == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
        
    }
    
    public function view(Request $request, $news_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $news = News::where('news_id', $news_id)->first();
        if ($news != null)
        {
        if ($news)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $news,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
        }
        elseif ($news == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
        
    }

    public function viewDecode(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
    
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }

        $news = News::where('event_code', $decodeToken->event_code)->get();
        if ($news)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $news,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }
}