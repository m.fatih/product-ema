<?php

namespace App\Http\Controllers;
use App\Quiz;
use App\Question;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use DB;

class QuestionController extends Controller
{
     public function index(Request $request)
     {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }

        $question = Question::all();
        if ($question)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $question,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function view(Request $request, $question_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }

        $question = Question::where('question_id', $question_id)->first();
        if ($question != null)
        {

        if ($question)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $question,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }
        elseif ($question == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function viewByQuiz(Request $request, $quiz_code)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }

        $question = Question::where('quiz_code', $quiz_code)->get();
        if ($question != null)
        {

        if ($question)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $question,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }
        elseif ($question == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function store(Request $request, $quiz_code)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true, 
                'Message'=>'Token Required',
                ],200);
        }


        $quiz = Quiz::where('quiz_code', $quiz_code)->first();
        
        $question = new Question;
        $question->quiz_code = $quiz->quiz_code;
        $question->pertanyaan = $request->pertanyaan;
        $question->jawaban_1 = $request->jawaban_1;
        $question->jawaban_2 = $request->jawaban_2;
        $question->jawaban_3 = $request->jawaban_3;
        $question->jawaban_4 = $request->jawaban_4;
        $question->jawaban_benar = $request->jawaban_benar;
        $question->point = $request->point;
        $question->time = $request->time;
        $question->save();

        if ($question)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $question,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function update(Request $request, $question_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true, 
                'Message'=>'Token Required',
                ],200);
        }

        $question = Question::where('question_id', $question_id)->first();

        if ($question != null)
        {
            $question->pertanyaan = $request->pertanyaan;
            $question->jawaban_1 = $request->jawaban_1;
            $question->jawaban_2 = $request->jawaban_2;
            $question->jawaban_3 = $request->jawaban_3;
            $question->jawaban_4 = $request->jawaban_4;
            $question->jawaban_benar = $request->jawaban_benar;
            $question->point = $request->point;
            $question->time = $request->time;
            $question->save();

            if ($question)
            {
                return response()->json([
                    'StatusCode'   =>  200,
                    'Error' => false,
                    'Message' => 'success',
                    'Data' => $question,
                ]);
            }
            else
            {
                return response()->json([
                    'StatusCode' => 400,
                    'Error' => true,
                    'Message' => 'fail',
                ]);
            }
        }
        elseif ($question == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function destroy(Request $request, $question_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }

        $question = Question::where('question_id', $question_id)->first();

        if ($question != null)
        {
            $question->delete();

        
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success'
            ]);
      
        }
        elseif ($question == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function activation(Request $request, $question_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $question = Question::find($question_id);

        if ($question != null)
        {
        
        if ($question->status  == 'Active')
        {
            $question->status = 'NonActive';
            $question->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'NonActive Success',
            ]);
        }
        elseif ($question->status  == 'NonActive')
        {
            $question->status = 'Active';
            $question->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'Active success',
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }
        elseif ($question == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }
}