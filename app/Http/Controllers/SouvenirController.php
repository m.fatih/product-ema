<?php

namespace App\Http\Controllers;
use App\Souvenir;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;
use Auth;

class SouvenirController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    private function getSouvenirCode()
    {
        $prefix = 'SOU';

        $sNextKode = "";
        $sLastKode = "";
        $value = Souvenir::orderBy('souvenir_id', 'desc')->first();
        if ($value != "") { // jika sudah ada, langsung ambil dan proses...
            $sLastKode = intval(substr($value->souvenir_code, 3)); // ambil 3 digit terakhir
            $sLastKode = intval($sLastKode) + 1; // konversi ke integer, lalu tambahkan satu
            $sNextKode = $prefix.date('ym').substr($sLastKode,4); // format hasilnya dan tambahkan prefix
        } else { // jika belum ada, gunakan kode yang pertama
            $sNextKode = $prefix.date('ym').'0001';
        }
        return $sNextKode;

        //return $prefix.'0001';
    }

    public function index(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $souvenir = Souvenir::all();
        if ($souvenir)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $souvenir,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function store(Request $request)
    {
        // Session::get
        // dd($request);
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
    
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }
        
        $image = $request->file('image');
        $new_souvenir = str_random(8).'-'.$image->getClientOriginalName();
        $image->move('image/Souvenir', $new_souvenir);

        $souvenir = new Souvenir;
        // dd($souvenir);
        $souvenir->event_code = $decodeToken->event_code;
        // dd($souvenir);
        $souvenir->souvenir_code = $this->getSouvenirCode();       
        $souvenir->title = $request->title;
        $souvenir->desc = $request->desc;
        // dd($souvenir);
        $souvenir->image = $new_souvenir;
        // dd($souvenir);
        $souvenir->save();
        
        if ($souvenir)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $souvenir,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function update(Request $request, $souvenir_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
        
        $souvenir = Souvenir::where('souvenir_id', $souvenir_id)->first();

        if ($souvenir != null)
        {
        if ($request->image != null)
        {
            $image = $request->file('image');
            $new_souvenir = str_random(8).'-'.$image->getClientOriginalName();
            $image->move('image/Souvenir', $new_souvenir);

            $souvenir->title = $request->title;
            $souvenir->desc = $request->desc;
            $souvenir->image = $new_souvenir;
        }
        else
        {
            $souvenir->title = $request->title;
            $souvenir->desc = $request->desc;
        }

        $souvenir->save();
        
        if ($souvenir)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $souvenir,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
        }
        elseif($souvenir == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function activation(Request $request, $souvenir_id)
    {
        // dd('oke');
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $souvenir = Souvenir::where('souvenir_id', $souvenir_id)->first();
        if ($souvenir != null)
        {
        if ($souvenir->status  == 'Active')
        {
            $souvenir->status = 'NonActive';
            $souvenir->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'NonActivation Success',
            ]);
        }
        elseif ($souvenir->status  == 'NonActive')
        {
            $souvenir->status = 'Active';
            $souvenir->save();

            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'Activation Success',
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
        }
        elseif ($souvenir == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function view(Request $request, $souvenir_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        $souvenir = Souvenir::where('souvenir_id', $souvenir_id)->first();
        if ($souvenir != null)
        {
        if ($souvenir)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $souvenir,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
        }
        elseif ($souvenir == null)
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }

    public function viewByEvent(Request $request)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');
        try{
            $decodeToken = JWT::decode($request->header('token'),env('JWT_SECRET'), array('HS256'));
        } 
    
        catch(\Exception $e){
            return response()->json([
                'StatusCode'=>'400',
                'Error'=>true,
                'Message'=>'Failed, Token Is Wrong!'
            ],422);
        }

        $souvenir = Souvenir::where('event_code', $decodeToken->event_code)->get();

        if ($souvenir)
        {
            return response()->json([
                'StatusCode'   =>  200,
                'Error' => false,
                'Message' => 'success',
                'Data' => $souvenir,
            ]);
        }
        else
        {
            return response()->json([
                'StatusCode' => 400,
                'Error' => true,
                'Message' => 'fail',
            ]);
        }
    }

    public function destroy(Request $request, $souvenir_id)
    {
        if(empty($request->header('token'))){
            return response()->json([
                'StatusCode'=>'400',
                'Error' =>true,
                'Message'=>'Token Required',
                ],200);
        }
        $token = $request->header('token');

        
        $souvenir = Souvenir::where('souvenir_id', $souvenir_id)->first();
       
        if ($souvenir != null )
        {
           
            $souvenir->delete();
        
            return response()->json([
                'StatusCode' => 200,
                'Error' => false,
                'Message' => 'success',
                
            ]);
        }
        elseif ($souvenir == null )
        {
            return response()->json([
                'StatusCode' => 404,
                'Error' => true,
                'Message' => 'ID Tidak Tersedia',
                
            ]);
        }
    }
}