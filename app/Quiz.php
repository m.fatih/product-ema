<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;


class Quiz extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'quiz_id';
    protected $table = 'quiz';
    protected $fillable = [
        'event_code',
        'quiz_code', 
        'name', 
        'jumlah_point', 
        'total_waktu', 
        'status'
    ];

    public function soal()
    {
        return $this->hasMany(Soal::class, 'quiz_id', 'soal_id');
    }
   
}