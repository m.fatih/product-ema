<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;


class Galery extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'galery_id';
    protected $table = 'galery';
    protected $fillable = [
        'event_code',
        'galery_title', 
        'galery_image', 
        'galery_desc', 
        'status'
    ];

   
}
