<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;


class Agenda extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'agenda_id';
    protected $table = 'agenda';
    protected $fillable = [
        'agenda_code',
        'event_code', 
        'event_title',
        'event_duration',
        'agenda_title',
        'agenda_desc',
        'event_month',
        'event_year',
        'event_date',
        'event_time',
        'event_speaker',
        'event_location_detail',
        'status'
    ];
}
