<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;


class Ticket extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'ticket_id';
    protected $table = 'ticket';
    protected $fillable = [
        'ticket_number',
        'event_code',
        'event_title',
        'event_start', 
        'event_end',
        'member_id',
        'member_name',
        'member_email',
        'member_phone_number',
        'member_image',
        'status'
    ];

   
}
