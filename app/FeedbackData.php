<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;


class FeedbackData extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'feedback_data_id';
    protected $table = 'feedback_data';
    protected $fillable = [
        'event_code',
        'rate',
        'member_id',
        'status'  
    ];

    

   
}