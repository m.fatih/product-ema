<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;


class News extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'news_id';
    protected $table = 'news';
    protected $fillable = [
        'event_code', 
        'news_code',
        'news_title',
        'news_image',
        'news_desc',
        'news_creator',
        'status'
    ];
}
