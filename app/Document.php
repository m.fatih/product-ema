<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;


class Document extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'document_id';
    protected $table = 'document';
    protected $fillable = [
        'event_code', 'event_file', 'file_desc', 'status'
    ];

   
}
